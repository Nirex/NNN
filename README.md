WELCOME
=======

Welcome to Nirex Neural Network (NNN)

INFO
====

### WHAT IS IT?

This is a minimalistic logistic regression neural network made completely in standard C++ using absolutely no third-party library.

### WHY DOES IT EXIST?

This tiny abstract neural network allows users to easily create a neural brain(or more) for their applications using logistic regression.

### PLANS

- May Add Convolutional Layers Later on

COPYRIGHT
=========

Copyright (C) 2018 Nirex

Please read the LICENSE file for License details and terms of use.

CONTACT
=======

Nirex.0@gmail.com
