//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _ACTIVATION_H_
#define _ACTIVATION_H_

enum class Activation
{
	Invalid,
	LiteSigmoid,
	Sigmoid,
	TanH,
	ReLU,
	LReLU,
	Exp
};

#endif // !_ACTIVATION_H_
