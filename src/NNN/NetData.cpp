#include "NetData.h"

NetData::NetData(const std::string filename)
{
	m_trainingDataFile.open(filename.c_str());
}

NetData::~NetData(void)
{
	m_trainingDataFile.close();
}

bool NetData::IsEof(void)
{
	return m_trainingDataFile.eof();
}

void NetData::GetTopology(std::vector<unsigned>& topology)
{
	std::string line;
	std::string label;

	std::getline(m_trainingDataFile, line);
	std::stringstream ss(line);
	ss >> label;
	if (this->IsEof() || label.compare("topology:") != 0) 
	{
		abort();
	}

	while (!ss.eof()) 
	{
		unsigned n;
		ss >> n;
		topology.push_back(n);
	}

	return;
}

unsigned NetData::GetNextInputs(std::vector<double>& inputVals)
{
	inputVals.clear();

	std::string line;
	std::getline(m_trainingDataFile, line);
	std::stringstream ss(line);

	std::string label;
	ss >> label;
	if (label.compare("in:") == 0) 
	{
		double oneValue;
		while (ss >> oneValue) {
			inputVals.push_back(oneValue);
		}
	}

	return inputVals.size();
}

unsigned NetData::GetTargetOutputs(std::vector<double>& targetOutputVals)
{
	targetOutputVals.clear();

	std::string line;
	std::getline(m_trainingDataFile, line);
	std::stringstream ss(line);

	std::string label;
	ss >> label;
	if (label.compare("out:") == 0) 
	{
		double oneValue;
		while (ss >> oneValue) {
			targetOutputVals.push_back(oneValue);
		}
	}

	return targetOutputVals.size();
}
