//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_NETDATA_H_
#define _N_NETDATA_H_

#include <vector>
#include <string>
#include <fstream>
#include <sstream>

class NetData
{
public:
	NetData(const std::string filename);
	~NetData(void);

	bool IsEof(void);
	void GetTopology(std::vector<unsigned>& topology);

	unsigned GetNextInputs(std::vector<double>& inputVals);
	unsigned GetTargetOutputs(std::vector<double>& targetOutputVals);

private:
	std::ifstream m_trainingDataFile;
};

#endif // !_N_NETDATA_H_