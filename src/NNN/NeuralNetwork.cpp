//� 2018 NIREX ALL RIGHTS RESERVED

#include <cassert>
#include "NeuralNetwork.h"
#include "NFile.h"

double NeuralNetwork::m_recentAverageSmoothingFactor = 100.0;

NeuralNetwork::NeuralNetwork(const std::vector<unsigned>& topology, Activation fType)
{
	assert(fType != Activation::Invalid);
	unsigned numLayers = topology.size();
	for (unsigned layerNum = 0; layerNum < numLayers; ++layerNum) 
	{
		m_layers.push_back(Layer());
		unsigned numOutputs = layerNum == topology.size() - 1 ? 0 : topology[layerNum + 1];

		for (unsigned neuronNum = 0; neuronNum <= topology[layerNum]; ++neuronNum)
		{
			m_layers.back().push_back(Neuron(numOutputs, neuronNum, fType));
		}

		m_layers.back().back().OutputVal(1.0);
	}
}

void NeuralNetwork::FeedForward(const std::vector<double>& inputVals)
{
	assert(inputVals.size() == m_layers[0].size() - 1);

	for (unsigned i = 0; i < inputVals.size(); i++) 
	{
		m_layers[0][i].OutputVal(inputVals[i]);
	}

	for (unsigned layerNum = 1; layerNum < m_layers.size(); layerNum++)
	{
		Layer &prevLayer = m_layers[layerNum - 1];
		for (unsigned n = 0; n < m_layers[layerNum].size() - 1; n++)
		{
			m_layers[layerNum][n].feedForward(prevLayer);
		}
	}
}

void NeuralNetwork::BackProp(const std::vector<double>& targetVals)
{
	Layer& outputLayer = m_layers.back();
	m_error = 0.0;

	for (unsigned n = 0; n < outputLayer.size() - 1; n++) 
	{
		double delta = targetVals[n] - outputLayer[n].OutputVal();
		m_error += delta * delta;
	}
	m_error /= outputLayer.size() - 1;
	m_error = sqrt(m_error); 

	m_recentAverageError = (m_recentAverageError * m_recentAverageSmoothingFactor + m_error) / (m_recentAverageSmoothingFactor + 1.0);

	for (unsigned n = 0; n < outputLayer.size() - 1; n++) 
	{
		outputLayer[n].calcOutputGradients(targetVals[n]);
	}

	for (unsigned layerNum = m_layers.size() - 2; layerNum > 0; layerNum--)
	{
		Layer& hiddenLayer = m_layers[layerNum];
		Layer& nextLayer = m_layers[layerNum + 1];

		for (unsigned n = 0; n < hiddenLayer.size(); n++)
		{
			hiddenLayer[n].calcHiddenGradients(nextLayer);
		}
	}

	for (unsigned layerNum = m_layers.size() - 1; layerNum > 0; layerNum--)
	{
		Layer& layer = m_layers[layerNum];
		Layer& prevLayer = m_layers[layerNum - 1];

		for (unsigned n = 0; n < layer.size() - 1; n++)
		{
			layer[n].updateInputWeights(prevLayer);
		}
	}
}

void NeuralNetwork::GetResults(std::vector<double>& resultVals) const
{
	resultVals.clear();

	for (unsigned n = 0; n < m_layers.back().size() - 1; n++) 
	{
		resultVals.push_back(m_layers.back()[n].OutputVal());
	}
}

double NeuralNetwork::GetRecentAverageError(void) const
{
	return m_recentAverageError;
}

void NeuralNetwork::SaveWeights(std::string path)
{
	std::vector<std::string> vfw;
	for (unsigned i = 0; i < m_layers.size(); i++)
	{
		for (unsigned j = 0; j < m_layers[i].size(); j++)
		{
			for (unsigned k = 0; k < m_layers[i][j].Weights().size(); k++)
			{
				vfw.push_back(std::to_string(m_layers[i][j].Weights()[k].weight));
			}
		}
	}
	NFile::WriteAllLines(path, vfw);
}

void NeuralNetwork::LoadWeights(std::string path)
{
	int iterator = 0;
	std::vector<std::string> vfw = NFile::ReadAllLines(path);
	for (unsigned i = 0; i < m_layers.size(); i++)
	{
		for (unsigned j = 0; j < m_layers[i].size(); j++)
		{
			for (unsigned k = 0; k < m_layers[i][j].Weights().size(); k++)
			{
				(m_layers[i][j].Weights()[k].weight) = std::stof(vfw[iterator]);
			}
		}
	}
}
