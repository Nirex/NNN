//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_NEURAL_NETWORK_H_
#define _N_NEURAL_NETWORK_H_

#include <vector>
#include <string>
#include "Neuron.h"
#include "Activation.h"

class NeuralNetwork
{
public:
	NeuralNetwork(const std::vector<unsigned>& topology, Activation fType);
	void FeedForward(const std::vector<double>& inputVals);
	void BackProp(const std::vector<double>& targetVals);
	void GetResults(std::vector<double>& resultVals) const;
	double GetRecentAverageError(void) const;

	void SaveWeights(std::string path);
	void LoadWeights(std::string path);

private:
	std::vector<Layer> m_layers;
	double m_error;
	double m_recentAverageError;
	static double m_recentAverageSmoothingFactor;
};

typedef NeuralNetwork Net;

#endif // !_N_NEURAL_NETWORK_H_