//� 2018 NIREX ALL RIGHTS RESERVED

#include "Neuron.h"
#include <cassert>

double Neuron::eta = 0.15;
double Neuron::alpha = 0.5;

Neuron::Neuron(unsigned numOutputs, unsigned myIndex, Activation fType)
{
	assert(fType != Activation::Invalid);
	for (unsigned c = 0; c < numOutputs; ++c) 
	{
		m_outputWeights.push_back(Connection());
		m_outputWeights.back().weight = randomWeight();
	}
	
	m_activationType = fType;
	m_myIndex = myIndex;
}

void Neuron::feedForward(const Layer& prevLayer)
{
	double sum = 0.0;

	for (unsigned n = 0; n < prevLayer.size(); ++n) 
	{
		sum += prevLayer[n].OutputVal() * prevLayer[n].m_outputWeights[m_myIndex].weight;
	}

	m_outputVal = Neuron::Activate(sum, m_activationType);
}

void Neuron::calcOutputGradients(double targetVal)
{
	double delta = targetVal - m_outputVal;
	m_gradient = delta * Neuron::DActivate(m_outputVal, m_activationType);
}

void Neuron::calcHiddenGradients(const Layer& nextLayer)
{
	double dow = sumDOW(nextLayer);
	m_gradient = dow * Neuron::DActivate(m_outputVal, m_activationType);
}

void Neuron::updateInputWeights(Layer& prevLayer)
{
	for (unsigned n = 0; n < prevLayer.size(); ++n) 
	{
		Neuron &neuron = prevLayer[n];
		double oldDeltaWeight = neuron.m_outputWeights[m_myIndex].deltaWeight;

		double newDeltaWeight = eta * neuron.OutputVal() * m_gradient + alpha * oldDeltaWeight;

		neuron.m_outputWeights[m_myIndex].deltaWeight = newDeltaWeight;
		neuron.m_outputWeights[m_myIndex].weight += newDeltaWeight;
	}
}

void Neuron::OutputVal(double val)
{
	m_outputVal = val;
}

double Neuron::OutputVal(void) const
{
	return m_outputVal;
}

std::vector<Connection> Neuron::Weights(void) const
{
	return m_outputWeights;
}

void Neuron::SetActivationType(Activation fType)
{
	this->m_activationType = fType;
}

double Neuron::Activate(double x, Activation fType)
{
	switch (fType)
	{
	case Activation::Invalid:
		return 0;
		break;
	case Activation::LiteSigmoid:
		return LiteSigmoid(x);
		break;
	case Activation::Sigmoid:
		return Sigmoid(x);
		break;
	case Activation::TanH:
		return TanH(x);
		break;
	case Activation::ReLU:
		return ReLU(x);
		break;
	case Activation::LReLU:
		return LReLU(x);
		break;
	case Activation::Exp:
		return Exp(x);
		break;
	default:
		return 0;
		break;
	}
	return 0;
}

double Neuron::DActivate(double x, Activation fType)
{
	switch (fType)
	{
	case Activation::Invalid:
		return 0;
		break;
	case Activation::LiteSigmoid:
		return DLiteSigmoid(x);
		break;
	case Activation::Sigmoid:
		return DSigmoid(x);
		break;
	case Activation::TanH:
		return DTanH(x);
		break;
	case Activation::ReLU:
		return DReLU(x);
		break;
	case Activation::LReLU:
		return DLReLU(x);
		break;
	case Activation::Exp:
		return DExp(x);
		break;
	default:
		return 0;
		break;
	}
	return 0;
}

double Neuron::LiteSigmoid(double x)
{
	return x / (1 + abs(x));
}

double Neuron::DLiteSigmoid(double x)
{
	return 1 / (pow(abs(x) + 1, 2));
}

double Neuron::Sigmoid(double x)
{
	return 1 / (1 + exp((double)-x));
}

double Neuron::DSigmoid(double x)
{
	double sigval = Sigmoid(x);
	return sigval * (1 - sigval);
}

double Neuron::TanH(double x)
{
	return tanh(x);
}

double Neuron::DTanH(double x)
{
	return 1.0 - (x * x);
}

double Neuron::ReLU(double x)
{
	if (x > 0.0)
		return x;
	return 0.00;
}

double Neuron::DReLU(double x)
{
	if (x > 0.0)
		return 1;
	if (x <= 0.0)
		return 0;
}

double Neuron::LReLU(double x)
{
	if (x > 0.0)
		return x;
	return 0.01;
}

double Neuron::DLReLU(double x)
{
	if (x > 0.0)
		return 1;
	if (x <= 0.0)
		return 0.01;
}

double Neuron::Exp(double x)
{
	return exp(x);
}

double Neuron::DExp(double x)
{
	return Exp(x);
}

double Neuron::randomWeight(void)
{
	return rand() / double(RAND_MAX);
}

double Neuron::sumDOW(const Layer& nextLayer) const
{
	double sum = 0.0;

	for (unsigned n = 0; n < nextLayer.size() - 1; ++n)
	{
		sum += m_outputWeights[n].weight * nextLayer[n].m_gradient;
	}

	return sum;
}
