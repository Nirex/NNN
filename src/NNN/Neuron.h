//� 2018 NIREX ALL RIGHTS RESERVED

#ifndef _N_NEURON_H_
#define _N_NEURON_H_

#include <vector>
#include <math.h>

#include "Connection.h"
#include "Activation.h"

class Neuron;

typedef std::vector<Neuron> Layer;

class Neuron
{
public:
	Neuron(unsigned numOutputs, unsigned myIndex, Activation fType);
	
	void feedForward(const Layer &prevLayer);
	void calcOutputGradients(double targetVal);
	void calcHiddenGradients(const Layer &nextLayer);
	void updateInputWeights(Layer &prevLayer);

	void OutputVal(double val);
	double OutputVal(void) const;

	std::vector<Connection> Weights(void) const;

	void SetActivationType(Activation fType);

private:
	static double Activate(double x, Activation fType);
	static double DActivate(double x, Activation fType);

	static double LiteSigmoid(double x);
	static double DLiteSigmoid(double x);

	static double Sigmoid(double x);
	static double DSigmoid(double x);

	static double TanH(double x);
	static double DTanH(double x);

	static double ReLU(double x);
	static double DReLU(double x);

	static double LReLU(double x);
	static double DLReLU(double x);

	static double Exp(double x);
	static double DExp(double x);

	static double randomWeight(void);

	double sumDOW(const Layer& nextLayer) const;

private:
	static double eta;
	static double alpha;

	double m_outputVal;
	std::vector<Connection> m_outputWeights;
	unsigned m_myIndex;
	double m_gradient;

	Activation m_activationType;
};

#endif // !_N_NEURON_H_