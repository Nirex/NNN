//� 2018 NIREX ALL RIGHTS RESERVED

#include "NeuralNetwork.h"

#include <vector>
#include <iostream>
#include <string>

void PrintArr(std::vector<float> in)
{
	for (size_t i = 0; i < in.size(); i++)
	{
		std::cout << in[i] << " ";
	}
	std::cout << "\n";
}

bool CompareVecs(std::vector<double> a, std::vector<double> b)
{
	if (a.size() == b.size())
	{
		for (size_t i = 0; i < a.size(); i++)
		{
			if (abs(a[i]) < 0.001f)
			{
				a[i] = 0;
			}
			else if (abs(a[i]) > 0.99f)
			{
				a[i] = 1;
			}

			if (abs(b[i]) < 0.001f)
			{
				b[i] = 0;
			}
			else if (abs(b[i]) > 0.99f)
			{
				b[i] = 1;
			}

			if (a[i] == b[i])
				continue;
			else
				return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}

void PrintVec(std::string label, std::vector<double> &v)
{
	std::cout << label << " ";
	for (unsigned i = 0; i < v.size(); ++i) {
		if (abs(v[i]) < 0.001f)
		{
			std::cout << "0 ";
		}
		else if (abs(v[i]) > 0.99f)
		{
			std::cout << "1 ";
		}
		else
		{
			std::cout << v[i] << " ";
		}
	}

	std::cout << std::endl;
}

auto main(int argc, char** argv) -> int
{
	std::vector<unsigned> topology;
	topology.push_back(5);
	topology.push_back(5);
	topology.push_back(5);

	Net* myNet = new Net(topology, Activation::TanH);

	std::vector<double> inputVals0;
	inputVals0.push_back(0);
	inputVals0.push_back(0);
	inputVals0.push_back(1);
	inputVals0.push_back(0);
	inputVals0.push_back(0);

	std::vector<double> inputVals1;
	inputVals1.push_back(1);
	inputVals1.push_back(1);
	inputVals1.push_back(0);
	inputVals1.push_back(1);
	inputVals1.push_back(1);

	std::vector<double> targetVals0;
	targetVals0.push_back(0);
	targetVals0.push_back(0);
	targetVals0.push_back(1);
	targetVals0.push_back(0);
	targetVals0.push_back(0);

	std::vector<double> targetVals1;
	targetVals1.push_back(1);
	targetVals1.push_back(1);
	targetVals1.push_back(0);
	targetVals1.push_back(1);
	targetVals1.push_back(1);

	std::vector<double> resultVals0;
	std::vector<double> resultVals1;

	int h = 0;
	unsigned long long epoch = 0;
	unsigned long long learn_epoch = 0;
	bool bHasReached = 0;
	while (true)
	{
		h++;

		myNet->FeedForward(inputVals0);
		myNet->GetResults(resultVals0);
		myNet->BackProp(targetVals0);

		myNet->FeedForward(inputVals1);
		myNet->GetResults(resultVals1);
		myNet->BackProp(targetVals1);
		epoch++;

		if (CompareVecs(targetVals0, resultVals0) && CompareVecs(targetVals1, resultVals1) && !bHasReached)
		{
			bHasReached = 1;
			learn_epoch = epoch;
		}

		if (CompareVecs(targetVals0, resultVals0) && CompareVecs(targetVals1, resultVals1))
		{
			std::cout << "Current Epoch: " << epoch << std::endl;
			std::cout << "Learn Epoch: " << learn_epoch << std::endl;
			PrintVec("Inputs 0:", inputVals0);
			PrintVec("Targets 0: ", targetVals0);
			PrintVec("Output 0: ", resultVals0);
			std::cout << std::endl;
			PrintVec("Inputs 1: ", inputVals1);
			PrintVec("Targets 1: ", targetVals1);
			PrintVec("Output 1 : ", resultVals1);

			std::cout << "Error: " << myNet->GetRecentAverageError() << std::endl;
			std::cout << "\n";
			h = 0;

			std::cin.get();
		}
	}

	std::cout << std::endl << "Done" << std::endl;
}